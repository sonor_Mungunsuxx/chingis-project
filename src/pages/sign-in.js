import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom'
import { AuthContext } from '../providers/auth-user-provider';
import { useFirebase } from '../Hooks/firebase';
import { Layout, FormInput, ActionButton } from '../components'

export const SignIn = () => {
    const history = useHistory();
    const { user } = useContext(AuthContext);
    const { auth, firebase } = useFirebase();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // const signUpPage = () => {
    //     history.push('./register')
    // }
    console.log(user);

    if (user) {
        history.push('/')
    }

    const handleChangeEmail = (e) => setEmail(e.target.value);
    const handleChangePassword = (e) => setPassword(e.target.value);


    const signIn = async () => {
        await auth.signInWithEmailAndPassword(email, password).catch((error) => {
            alert(error.message);
        })
    }

    const facebook = () => {
        var provider = new firebase.auth.FacebookAuthProvider();
    
        auth.signInWithPopup(provider).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
            console.log(token);
            console.log(user);
        }).catch(function(error) {
            var errorMessage = error.message;
            alert(errorMessage)
        });
    }

    const google = () => {
        var provider = new firebase.auth.GoogleAuthProvider();


        auth.signInWithPopup(provider).then((result) => {
            var token = result.credential.accessToken;
            var user = result.user;

            console.log(token);
            console.log(user);

          }).catch(function(error) {
            var errorMessage = error.message;

            console.log(errorMessage)
          });
    }


    return (
        <Layout>
            <div className='items-center'>
                <div>
                    <p className='title'>Нэвтрэх</p>
                    <FormInput label='Цахим хаяг' placeholder='name@mail.domain' value={email} onChange={handleChangeEmail} />
                    <div className='mt-4'></div>
                    <FormInput label='Нууц үг' type='password' placeholder='Password' value={password} onChange={handleChangePassword} />
                    <ActionButton onClick={signIn}>Нэвтрэх</ActionButton>
                </div>
                <div className='mt-30'>
                    <ActionButton onClick={facebook}>Facebook</ActionButton>
                    <ActionButton onClick={google}>Google</ActionButton>
                </div>
            </div>
        </Layout>
    )
}
