import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { useFirebase } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider'
import { FormInput, ActionButton, Stack, Layout } from '../components'

export const SignUp = () => {
    const { user } = useContext(AuthContext)
    const [state, setState] = useState({ email: '', password: '', password2: '', username: '', phone: '', groupName: '', groupMail: '', medku: '' });
    const history = useHistory();
    const [error, setError] = useState('');
    const { firebase, auth, firestore } = useFirebase();

    const [tab, setTab] = useState('Individual');

    const handleChangeUsername = (e) => setState({ ...state, username: e.target.value })
    const handleChangeEmail = (e) => setState({ ...state, email: e.target.value });
    const handleChangePassword = (e) => setState({ ...state, password: e.target.value });
    const handleChangePassword2 = (e) => setState({ ...state, password2: e.target.value });

    const handleChangeGroupName = (e) => setState({ ...state, groupName: e.target.value });
    const handleChangeGroupMail = (e) => setState({ ...state, groupMail: e.target.value });
    const handleChangeMedku = (e) => setState({ ...state, medku: e.target.value });

    if (user) {
        history.push('/')
    }
    console.log(error);

    const facebook = () => {
        var provider = new firebase.auth.FacebookAuthProvider();

        auth.signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            console.log(token);
            console.log(user);
            console.log(user.emailVerified)

            firestore.collection('users').doc(user.uid).set({
                username: user.displayName,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                logged: "Facebook",
                profileImage: "default"
            });
            history.push('/')

        }).catch(function (error) {
            var errorMessage = error.message;
            alert(errorMessage)
        });
    }

    const google = () => {
        var provider = new firebase.auth.GoogleAuthProvider();

        auth.signInWithPopup(provider).then((result) => {
            var token = result.credential.accessToken;
            var user = result.user;

            console.log(token);
            console.log(user);

            console.log(user.emailVerified)

            firestore.collection('users').doc(user.uid).set({
                username: user.displayName,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                logged: "Google",
                profileImage: "default"
            });

            if (user.emailVerified === true) {
                history.push('/')
            } else {
                history.push('/Verify')
            }
        }).catch(function (error) {
            var errorMessage = error.message;

            console.log(errorMessage)
        });
    }

    const isFilled = () => {
        if (state.username === '')
            return true;
        if (state.email === '')
            return true;
        if (state.password === '')
            return true;
        if (state.password2 === '')
            return true;
        return false
    }


    const signUp = async () => {
        if (tab !== 'Group') {
            if (!(state.email && state.password && state.password2)) {
                setError('Please enter all the required information');
                return;
            }
            if (state.password !== state.password2) {
                setError('Passwords dont match!');
                return;
            }
            let cred = await auth.createUserWithEmailAndPassword(state.email, state.password)
                .catch((error) => {
                    alert(error.message)
                })

            await firestore.collection('users').doc(cred.user.uid).set({
                username: state.username,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                logged: "default",
                profileImage: "default"
            });

            history.push('/Verify')
        } else {
            if (!(state.groupName && state.groupMail && state.medku)) {
                setError('Please enter all the required information');
                return;
            }
            let group = await auth.createUserWithEmailAndPassword(state.groupMail, state.medku)
                .catch((error) => {
                    alert(error.message)
                })

            await firestore.collection('users').doc(group.user.uid).set({
                username: state.groupName,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                logged: "Group",
                profileImage: "default",
            });

        }

    }

    return (
        // <div className="pa-24 text-center font-Raleway"> 
        //     <div>
        //         <div className="mb-70 gap-12">
        //         <div className="font-Raleway bold">Бүртгүүлэх</div>
        //         <div className="c-gray3 font-Raleway">Та өөрийн бүртгэлээ үүсгэнэ үү? </div>
        //         </div>
        //         <div>
        //             <div className="flex-row br-primary-2 bold font-Raleway mb-20 mt-10 bradius-10">
        //                 {
        //                     (tab === 'Individual') ? <div onClick={() => { setTab('Individual') }} className="pa-10 text-center b-primary flex-1">Individual</div> : <div onClick={() => { setTab('Individual') }} className="pa-10 text-center flex-1">Individual</div>
        //                 }
        //                 {
        //                     (tab === 'Group') ? <div onClick={() => { setTab('Group') }} className="pa-10 text-center b-primary flex-1">Group</div> : <div onClick={() => { setTab('Group') }} className="pa-10 text-center flex-1">Group</div>
        //                 }
        //             </div>
        //         </div>
        //         {
        //             (tab === 'Individual') ? <div className="grid rowGap-16">
        //                 <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Хэрэглэгчийн нэр' type='text' icon='user' placeholder='Username' value={state.username} onChange={handleChangeUsername} />
        //                 <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Цахим хаяг' placeholder='name@mail.com' type='email' value={state.email} onChange={handleChangeEmail} />
        //                 <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Нууц үг' placeholder='Password' type='password' value={state.password} onChange={handleChangePassword} />
        //                 <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Нууц үгээ давтна уу?' placeholder='Password' type='password' value={state.password2} onChange={handleChangePassword2} />
        //                 <ActionButton className="h-50 mt-15 c-primaryfourth b-primary brad-5" disabled={isFilled()} icon="signupin" onClick={signUp}>Бүртгүүлэх</ActionButton>
        //             </div>
        //                 :
        //                 <div className="rowGap-16">
        //                     <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Group нэр' type='text' placeholder='Group Name' value={state.groupName} onChange={handleChangeGroupName} />
        //                     <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Цахим хаяг' type='email' placeholder='company@mail.com' value={state.groupMail} onChange={handleChangeGroupMail} />
        //                     <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Цахим хаяг' type='password' placeholder='Medku' value={state.medku} onChange={handleChangeMedku} />
        //                     <ActionButton className="h-50 mt-15 c-primaryfourth b-primary brad-5" disabled={isFilled()} icon="signupin" onClick={signUp}>Бүртгүүлэх</ActionButton>

        //             </div>
        //         }

        //         <div className="mt-24">
        //         <div>Нууц үг мартсан? 
        //             <ActionButton className="text-button c-primary">Энд дарна уу</ActionButton>
        //         </div>
        //         </div>
        //         {/* <div>
        //             <div className=" ">Бүртгүүлэх</div>
        //             <div className="c-gray3">Та өөрийн бүртгэлээ үүсгэнэ үү? </div>
        //         <div>
        //         <FormInput label='Full Name' type='text' placeholder='Username' value={state.username} onChange={handleChangeUsername} />
        //         <FormInput label='Email Address' placeholder='name@mail.com' type='email' value={state.email} onChange={handleChangeEmail} />
        //         <FormInput label='Password' placeholder='Password' type='password' value={state.password} onChange={handleChangePassword} />
        //         <FormInput label='Нууц үгээ давтна уу?' placeholder='Password' type='password' value={state.password2} onChange={handleChangePassword2} />

        //         {error && <div>{error}</div>}
        //         <div className="pa-16">
        //         <ActionButton className="h-50 c-primaryfourth b-primary brad-5" icon="signupin" onClick={signUp}>Бүртгүүлэх</ActionButton>
        //         <div>Нууц үг мартсан?</div>

        //         </div> */}

        //     {/* </div> */}
        //     {
        //         (tab !== 'Group') ?
        //         <div>Эсвэл үүгээр бүртгүүлж болно
        //         <div className="flex-row gap-0px-7px">
        //         {/* <ActionButton className="h-50 b-fb-color c-primaryfourth brad-5 pa-12" icon='facebook' onClick={facebook}>Facebook</ActionButton>       */}
        //         <ActionButton className="h-50 b-gmail-color c-primaryfourth brad-5 pa-12" icon='gmail' onClick={google}>Google хаягаар нэвтрэх</ActionButton>
        //     </div>
        //     </div>
        //         :
        //         <div></div>
        //     }




        // {/* </div> */}
        // </div>
        // </div>


        <Layout>
            <div className="font-main w90 text-center font-Raleway">
                <div className="flex-col justify-between" style={{height: 'calc(100vh - 100px)'}}>
                    <div className="">
                        <div class="font-main bold fs-24">Бүртгүүлэх </div>
                        <div className="c-gray3 font-Raleway fs-16 normal mt-24">Та өөрийн бүртгэлээ үүсгэнэ үү? </div>
                        
                    </div>


                    <Stack size={4}>
                        <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Хэрэглэгчийн нэр' type='text' icon='user' placeholder='Username' value={state.username} onChange={handleChangeUsername} />
                        <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Цахим хаяг' placeholder='name@mail.com' type='email' value={state.email} onChange={handleChangeEmail} />
                        <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Нууц үг' placeholder='Password' type='password' value={state.password} onChange={handleChangePassword} />
                        <FormInput className="bradius-5 ph-44 h-50 br-border-color-1" label='Нууц үгээ давтна уу?' placeholder='Password' type='password' value={state.password2} onChange={handleChangePassword2} />
                        <ActionButton className="font-main h-50 c-primaryfourth b-primary brad-5" disabled={isFilled()} icon="signupin" onClick={signUp}>Бүртгүүлэх</ActionButton>


                    <div className="flex justify-center">
                        <div>Нууц үг мартсан?</div>
                        <div className="ml-5 text-button c-primary bold">Энд дарна уу</div>
                    </div>
                    </Stack>


                    
                    <div className="flex-col">
                        <div className="mb-24 c-gray5">Эсвэл үүгээр бүртгүүлж болно</div>
                        {/* <ActionButton className="h-50 b-fb-color c-primaryfourth brad-5 pa-12" icon='facebook' onClick={facebook}>Facebook</ActionButton>       */}
                        <ActionButton className="font-main mt-6 h-50 b-gmail-color c-primaryfourth brad-5 pa-12" icon='gmail' onClick={google}>Google хаягаар бүртгүүлэх</ActionButton>
                    </div>
                </div>
            </div>
        </Layout>
    )
}
