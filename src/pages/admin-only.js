import React, { useContext } from 'react';
import {AuthContext} from '../providers/auth-user-provider'
import { Stack, SearchInput } from '../components'
import { useDoc } from '../Hooks';
import { useHistory } from 'react-router-dom'

export const Adminz = () => {
    const history = useHistory();
    const { user } = useContext(AuthContext);
    let uid;

    if (user != null) {
        uid = user.uid
    }

    const { data } = useDoc(`users/${uid}`);

    // if(data && !data.adminz) {
    //     history.push('/feed')
    // } 

    return (
        <div className="h-vh-100">
            <Stack size={5}>
                <div className="mt-30 mb-10 flex-center">
                    <div className="lh-31 fw700 fs-24 font-main">Verify Account</div>
                </div>
                <SearchInput />

            </Stack>
        </div>
    )
}