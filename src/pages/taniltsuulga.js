import React, { useContext, useState } from 'react'
import { ActionButton, Button, Layout, Tanil1 } from '../components'
import { useHistory } from 'react-router-dom'
import {AuthContext} from '../providers/auth-user-provider'

export const Taniltsuulga = () => {
    const history = useHistory();
    const { user } = useContext(AuthContext)
    const [iconNumber, setIconNumber] = useState(0)

    if(user) {
        history.push('/feed')
    }
    return (
        <Layout color={false}>
            <div className='flex-col justify-between'>
                <div className='flex items-center flex-col'>
                    <Tanil1></Tanil1>
                    <div className='flex-row justify-between w-50 ma-20'>
                        {
                            (iconNumber == 0) ?
                                <div className='w-8 h-8 b-primary bradius-20'></div>
                                : <div className='w-8 h-8 b-gray2 op bradius-20' onClick={() => { setIconNumber(0) }}></div>
                        }
                        {
                            (iconNumber == 1) ?
                                <div className='w-8 h-8 b-primary bradius-20'></div>
                                : <div className='w-8 h-8 b-gray2 op bradius-20' onClick={() => { setIconNumber(1) }}></div>
                        }
                        {
                            (iconNumber == 2) ?
                                <div className='w-8 h-8 b-primary bradius-20'></div>
                                : <div className='w-8 h-8 b-gray2 op bradius-20' onClick={() => { setIconNumber(2) }}></div>
                        }
                    </div>

                    <div className='mt-50'>
                        {(iconNumber == 0) && <h1 className='font-main'>Sanal ogoh</h1>}
                        {(iconNumber == 1) && <h1 className='font-main'>Sanal tsugluulah</h1>}
                        {(iconNumber == 2) && <h1 className='font-main'>Tender zarlah</h1>}

                    </div>

                    {
                        iconNumber != 2 ?
                            <ActionButton className='w-100 h-37 b-primary bradius-15 mt-110' onClick={() => { setIconNumber(iconNumber + 1) }} >Next</ActionButton>
                            :
                            <ActionButton className='w-100 h-37 b-primary bradius-15 mt-110' onClick={() => { history.push('/register') }}>Sign Up</ActionButton>
                    }
                </div>
                <div className='w-vw-95 flex justify-end mt-50'>
                    <h3 className='font-main' onClick={() => {history.push('/feed')}}>Algasah</h3>                 
                </div>
            </div>
        </Layout>
    )
}