import React, { useEffect, useContext, useRef, useState } from 'react';
import { useDoc, useCol, useFirebase } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider';
import { useStorage } from '../Hooks';
import { Button, GmailIcon, Layout } from '../components';
import { callIcon } from '../components/icons'
import { useHistory } from 'react-router-dom';
import { Stack, Box } from '../components';
import { RenderProfilePost } from '../components/render-profile-post';
import { Loader } from '../components/loader';
import { Verify } from './verify-account';

const Profile = (vote, voteCount) => {
    let oid = new URLSearchParams(window.location.search).get('user');
    const { user } = useContext(AuthContext)
    const { uid } = user || {};
    const history = useHistory();
    const { data, updateRecord, loading: eventLoading } = useDoc(`users/${oid ? oid : uid}`);
    const { data: createdPosts } = useCol(`users/${oid ? oid : uid}/createdEvents`);
    const { data: allEventInfo } = useCol(`Events`)
    const { data: userData } = useDoc(`users/${uid}`);
    const { profileImage, username } = data || {};
    const [file, setFile] = useState('')
    const [imgSrc, setImgSrc] = useState('')
    const inputFile = useRef(null);
    const [show, setShow] = useState(false)
    const [appear, setAppear] = useState(false)
    const { firebase } = useFirebase();
    const profileImageSrc = useStorage(`profileImages/${oid ? oid : uid}/profileImage.jpg`)
    
    const gmailShow = () => {
        setShow(!show)
    }

    const callShow = () => {
        setAppear(!appear)
    }


    useEffect(() => {
        if (inputFile.current) {
            function onFileChange() {
                setFile(inputFile.current.files[0]);
                console.log(inputFile)
                setImgSrc(URL.createObjectURL(inputFile.current.files[0]))
            }

            inputFile.current.addEventListener('change', onFileChange);
        }
    }, [inputFile]);

    useEffect(() => {
        if (firebase && file && uid) {
            console.log("aafd")
            var storageSideRef = firebase.storage().ref().child(`profileImages/${uid}/profileImage.jpg`);
            storageSideRef.put(inputFile.current.files[0])
                .then((snapshot) => {
                    console.log('Done. Storage');
                    updateRecord({ profileImage: "img" })
                    console.log('Done. Firestore')
                })
        }
    }, [firebase, file, uid, imgSrc, updateRecord])

    if (eventLoading) {
        return <Loader />
    }

    if (userData) {
        console.log(((userData && userData.logged === 'Group') || (userData && userData.id === uid)))
        console.log('========', userData)    
    }

    return (
        <div className='h-vh-100 tempCol-0-4 b-gray4'>
            <div className="flex flex-col b-default">
                <Layout />
                <Stack size={5} className="b-gray4">
                    <Box className="pv-20 pa-10" type='bottom'>
                        <div style={{ borderRadius: '100%', backgroundColor: '#9C9C9C', backgroundSize: 'cover', backgroundImage: `url("${imgSrc === '' ? profileImageSrc : imgSrc}")` }} className='flex-center bradius-10 w-100 h-100 margin-auto' >
                            <p className='op fs-24 font-main'>{(profileImage === "default") && (imgSrc === '') ? username[0] : ''}</p>
                        </div>
                        <div className="flex-center">
                            <h3 className=" m-10 mr-20 ml-20 font-main">{(data && data.username) || 'Нэргүй'}</h3>
                            {
                                
                            }
                        </div>

                        <input type='file' id='file' ref={inputFile} style={{ display: 'none' }} />

                    <input type='file' id='file' ref={inputFile} style={{ display: 'none' }} />
                    <Stack size={3}>
                            {
                                !oid &&
                                <Button onClick={() => { inputFile.current.click() }} className="btn-pf flex-center flex bradius-5 fs-14 bold font-main"> Edit profile</Button>
                            }
                            {((userData && userData.logged === 'Group') || (userData && userData.id === uid)) &&
                                <div className="flex-row justify-between space-around">
                                    <Button onClick={gmailShow} className="btn bradius-5 bold fs-14 h-25 w-880 "> Gmail</Button>
                                    <Button onClick={callShow} className="btn bradius-5 bold fs-14 h-25 "> Call</Button>
                                    <Button onClick={() => { history.push('/vote-history') }} className="btn bradius-5 bold fs-14 h-25 ">History</Button>
                                </div>
                            }
                            {show &&
                                <>
                                    <div className=" flex flex-center relative fs-16 font-main t-45 w-vw-90 bshadow bradius-10 b-default">
                                        <div className="text-center font-main mt-5 fw700 bt-primary-1">Gmail</div>
                                        <div className="bt-gray4-1 flex-row flex">
                                            <GmailIcon width={16} height={16} />
                                            <div className=""></div>
                                        </div>
                                    </div>
                                </>}
                            {appear &&
                                <>
                                    <div className=" flex flex-center relative fs-16 font-main t-45 w-vw-90 bshadow bradius-10 b-default">
                                        <div className="text-center font-main mt-5 fw700 bt-primary-1">Call</div>
                                        <div className="bt-gray4-1 flex-row flex">
                                            <callIcon width={16} height={16} />
                                            <div className=""></div>
                                        </div>
                                    </div>
                                </>}
                    </Stack>
                    </Box>

                    <Box className="pv-20">
                        {
                            data && data.logged === 'Group' ?
                                <div>
                                    <p className="font-main fs-16">Та өөрийн компани хаягаа баталгаат хаяг болгоx bol <span className="c-primary" onClick={() => <Verify/>}>End darna</span> уу !</p>
                                </div>
                                :
                                <></>
                        }
                    </Box>
                    <div className="ml-30 fs-24 fw-700 font-main">Your Posts</div>
                    {
                        allEventInfo && allEventInfo.filter((event) => !createdPosts.every((post) => post.id !== event.id)).map((e) => <Box className="pv-20"><RenderProfilePost key={e.id} {...e} /></Box>)
                    }
                </Stack>
            </div>
        </div>
    )
}

export default Profile;
