import React, { useState, useRef, useContext } from 'react'
import { Stack, MenuIcon, ProfileIcon, NewProjectIcon, XIcon, HomeIcon, LogOut } from '../components'
import { DownArrow } from './icons';
import { useHistory } from 'react-router-dom'
import { useOutsideClick } from '../Hooks/use-outside-click'
import { useFirebase } from '../Hooks/firebase'
import { AuthContext } from '../providers/auth-user-provider'

export const DropdownItem = ({ children, onClick, ...others }) => {
    return (
        <div className='flex-row items-center' onClick={onClick} {...others}>
            {children}
        </div>
    )
}

export const DropdownItemArrow = ({ children, onClick, ...others }) => {
    return (
        <div className='flex-center flex-row' onClick={onClick} {...others}>
            {children}
        </div>
    )
}

export const DropdownOptions = ({ children }) => {
    return (
        <div className='absolute b-default pa-vw-5 w-vw-80 l-0 margin-auto r-0 bshadow h-200 z-i bradius-10' size={4}>
            {children}
        </div>
    )
}

export const DropdownOptionsArrow = ({ children }) => {
    return (
        <Stack className='text-center font-DmSans b-default br-inactive-1 bradius-10' size={2}>
            {children}
        </Stack>
    )
}


export const DropdownMenu = () => {
    const history = useHistory();
    const [show, setShow] = useState(false);
    const ref = useRef();
    const { auth } = useFirebase();
    const { user } = useContext(AuthContext);
    let uid;

    if (user) {
        uid = user.uid;
    }

    useOutsideClick(ref, () => { setShow(false) });

    return (
        <div className='z-i bradius-10'>
            <div className='container'>
                {!show && <MenuIcon height={12} width={18} onClick={() => setShow(true)} className="mr-15" />
                }
                {show &&
                    <DropdownOptions>
                        <XIcon className='flex justify-end' height={15} width={15} onClick={() => setShow(false)} />
                        <div className="flex-col justify-between flex" style={{ height: '90%' }}>
                            <DropdownItem onClick={() => { history.push('/addEvent') }}>
                                <NewProjectIcon height={20} width={20} />
                                <div className='ml-20 fs-18 font-main'>Шинэ аян үүсгэх</div>
                            </DropdownItem>
                            <DropdownItem onClick={() => { history.push('/profile') }}>
                                <ProfileIcon height={20} width={20} />
                                <div className='ml-20 fs-18 font-main'>Хэрэглэгч</div>
                            </DropdownItem>
                            <DropdownItem onClick={() => { history.push('/') }}>
                                <HomeIcon height={20} width={20} />
                                <div className='ml-20 fs-18 font-main'>Нүүр хуудас</div>
                            </DropdownItem>
                            <DropdownItem onClick={() => { auth.signOut() }}>
                                <LogOut height={20} width={20} />
                                <div className='ml-20 fs-18 font-main c-warning'>Log Out</div>
                            </DropdownItem>
                        </div>
                    </DropdownOptions>}
            </div>
        </div>
    )
}

export const DropdownMenuArrow = ({ type, setType }) => {
    const [show, setShow] = useState(false);
    const ref = useRef()

    useOutsideClick(ref, () => { setShow(false) });

    return (
        <div className='pr font-main w-150 display-block mr-15' ref={ref}>
            {!show && <div className="fs-16 lh-21 text-right mt-12 flex bold justify-end items-center w100" onClick={() => { setShow((show) => !show) }}>
                {type}
                <DownArrow className="ml-10 mt-6 mr-5" height={18} width={12} />
            </div>}
            {show &&
                <div className='pr font-main w-150 display-block' ref={ref}>
                    <div className="fs-16 lh-21 text-right mt-12 flex bold justify-end items-center w100" onClick={() => { setShow((show) => !show) }}>
                        <div className='flex items-center'>
                            <div className='fs-16 lh-21 text-right display-block justify-end items-center'>Sort By {type}</div>
                            <DownArrow className="ml-10 mt-6 mr-5" height={18} width={12} />
                        </div>
                    </div>
                    <div className='absolute w100'>
                        <DropdownOptionsArrow>
                            <DropdownItemArrow onClick={() => setType('New')}>
                                <div className='mt-15 mb-5'>New</div>
                            </DropdownItemArrow>
                            <div className="b-inactive w100 h-1" />
                            <DropdownItemArrow onClick={() => setType('Popular')}>
                                <div className='mb-15 mt-5'>Popular</div>
                            </DropdownItemArrow>
                        </DropdownOptionsArrow>
                    </div>
                </div>}
        </div>
    )
}