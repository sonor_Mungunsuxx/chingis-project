const provider = new firebase.auth.GoogleAuthProvider();
firebase.auth().signInWithPopup(provider)
.catch(error => {
  console.log(error);
});

let callback = null;
let metadataRef = null;
firebase.auth().onAuthStateChanged(user => {
  if (callback) {
    metadataRef.off('value', callback);
  }
  if (user) {
    metadataRef = firebase.database().ref('metadata/' + user.uid + '/refreshTime');
    callback = (snapshot) => {
      user.getIdToken(true);
    };
    metadataRef.on('value', callback);
  }
});