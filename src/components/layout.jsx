import React from 'react';
import { Navigation } from '../pages/navigation';

export const Layout = ({ children, color }) => {

    return (
        <div>
            {
                color == true ?
                    <div className='containerpf b-gray6'>
                        <Navigation />
                        {children}
                    </div>
                : 
                <div className='containerpf'>
                    <Navigation />
                    {children}
                </div>
            }
        </div>
    )
}