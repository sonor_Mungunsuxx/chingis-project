import React from 'react';
import _ from 'lodash';

const fibonacci = _.memoize(n => {
    return n < 2 ? n : fibonacci(n - 1) + fibonacci(n - 2);
});

export const Grid = ({ children, className, size = 'small', columns = 'auto-fill' }) => {
    const baseSpace = 3;
    let style = {
        grid: {
            display: `grid`,
            gridGap: baseSpace * 3,
        },
        small: {
            gridTemplateColumns: `repeat(${columns}, minmax(${baseSpace *
                fibonacci(8)}px, 1fr))`,
        },
        medium: {
            gridTemplateColumns: `repeat(${columns}, minmax(${baseSpace *
                fibonacci(9)}px, 1fr))`,
        },
        large: {
            gridTemplateColumns: `repeat(${columns}, minmax(${baseSpace *
                fibonacci(10)}px, 1fr))`,
        },
    };

    return <div className={`${className}`} style={{...style.grid, ...style[size]}}>{children}</div>;
};