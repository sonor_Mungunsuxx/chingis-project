import React, { useState } from 'react';
import { DotIcon, TrashIcon, Button } from '../components';
import { useHistory } from 'react-router-dom'

export const RenderProfilePost = ({vote, voteCount, name, desc, mainImageUrl, id }) => {

    const [show, setShow] = useState(false);
    const [more, setMore] = useState(false);
    const history = useHistory();

    const handleShow = () => {
        
        setShow(!show)
    }

    return (
        <div className='pr'>
            <div className='flex justify-between items-center'>
                <div className='font-main fs-24 fw700'>{name}</div>
                <DotIcon className="pa-5" width={25} height={25} onClick={handleShow} />
            </div>
            <div onCLick={() => {history.push(`event-id?id=${id}`)}} style={{ backgroundColor: 'lightgray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${mainImageUrl}")` }} className='bradius-10 w100 h30' ></div>
            <div className='pr wbreak'>
                <div className='w100 absolute bradius-10 b-gray4 h-10 mt-2'></div>
                <div className='absolute bradius-10 b-primary h-10 mt-2' style={{ width: `${(vote / voteCount) * 100 || 5}%` }}></div>
            </div>
            {
                desc.length > 150 ?
                    <p className="wbreak font-main">{desc.substring(0, 150)}{!more ? <span className='ul c-primary' onClick={() => { setMore(true) }}> ... see more</span> : <span onClick={() => { setMore(true) }}>{desc.substring(150, desc.length - 1)}</span>} </p>
                    :
                    <p className="wbreak font-main">{desc}</p>
            }


            {show &&
                <>
                    <Button className="btn h-40 fs-16 font-main margin-auto t-45 w-vw-90 bshadow bradius-10 absolute b-default c-warning-second">Delete Post</Button>
                </>}
        </div>
    )
}