import React from 'react'

export const GmailIcon = ({ height, width, ...others }) => {
    return (
        <span {...others} >
            <svg width={width} height={height} viewBox="0 0 19 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M18.4287 3.14289C18.4287 2.16075 17.6251 1.35718 16.643 1.35718H2.35725C1.3751 1.35718 0.571533 2.16075 0.571533 3.14289M18.4287 3.14289V13.8572C18.4287 14.8393 17.6251 15.6429 16.643 15.6429H2.35725C1.3751 15.6429 0.571533 14.8393 0.571533 13.8572V3.14289M18.4287 3.14289L9.50011 9.39289L0.571533 3.14289" stroke="#52575C" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}