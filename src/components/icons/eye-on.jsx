import React from 'react'

export const EyeOnIcon = ({height, width, color = "#999999", ...others}) => {

    return (
        <span {...others}>
          <svg width={width} height={height} viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.571289 8.00021C0.571289 8.00021 3.99986 1.14307 9.99986 1.14307C15.9999 1.14307 19.4284 8.00021 19.4284 8.00021C19.4284 8.00021 15.9999 14.8574 9.99986 14.8574C3.99986 14.8574 0.571289 8.00021 0.571289 8.00021Z" stroke="#999999" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M9.99986 10.5716C11.42 10.5716 12.5713 9.42037 12.5713 8.00021C12.5713 6.58005 11.42 5.42878 9.99986 5.42878C8.5797 5.42878 7.42843 6.58005 7.42843 8.00021C7.42843 9.42037 8.5797 10.5716 9.99986 10.5716Z" stroke="#999999" strokeLinecap="round" strokeLinejoin="round"/>
        </svg>
        </span>
    )
}
