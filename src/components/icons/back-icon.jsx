import React from 'react'

export const BackIcon = ({ height, width, color = "#999999", ...others }) => {
    return (
        <span {...others} >
            <svg width={width} height={height} viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.53906 12.5L0.539062 6.5L6.53906 0.5" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}